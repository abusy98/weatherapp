# WeatherApp

- To run this project you need to install following pods
    1.  pod 'MBProgressHUD', '~> 1.0.0'
    2. pod 'ReachabilitySwift', '~> 3'
- Based on user's device location, this app will display the weather inforomation along with future five days forecast.
- You can add the location to favourite list and check the information offline
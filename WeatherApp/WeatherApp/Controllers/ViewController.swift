//
//  ViewController.swift
//  WeatherApp
//
//  Created by Neelesh Rai on 27/11/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var stateNameLabel: UILabel!
    @IBOutlet weak var tempratureIndicatorLabel: UILabel!
    @IBOutlet weak var stateTempratureLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel!
    @IBOutlet weak var lastUpdateTimeLabel: UILabel!
    @IBOutlet weak var addToFavButton: UIButton!
    @IBOutlet weak var showFavListButton: UIButton!
    @IBOutlet weak var refreshDataButton: UIButton!

    var displayItemsArray = [List]()
    var currentDayAndTime: Int = 0
    var currentInfoModel: WeatherInfoResponseModel?
    var forecastInfoModel: ForecastInfoModel?
    var favItemsArray = [[String: Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //Check for Offline Data
        if !Utility.isNetworkAvailable() {
            if let favData = UserDefaults.standard.object(forKey: "FavItems") as? Data {
                self.favItemsArray.removeAll()
                let decoder = JSONDecoder()
                if let offlineData = try? decoder.decode(WeatherInfoResponseModelOffline.self, from: favData), let curInfoModel = offlineData.current, let forecInfoModel = offlineData.forecast, let favStatus = offlineData.isFav {
                    if favStatus == "true" {
                        self.addToFavButton.setTitle("UnFav", for: .normal)
                    }
                    let dict = ["currentInfo": curInfoModel as Any, "forecastInfo": forecInfoModel as Any, "isFav": favStatus] as [String : Any]
                    self.favItemsArray.append(dict)
                    self.reloadViewWithOfflineData(withCurrentWeatherInfo: curInfoModel, andForecastInfo: forecInfoModel)
                }
            } else {
                Utility.sharedInstance.showToast(message: "Please switch on the internet.")
            }
        } else {
            let NothaveAccess = checkLocationAccessStatus()
            Utility.showLoader()
            if NothaveAccess {
                NotificationCenter.default.addObserver(self, selector: #selector(handleLocationManagerSelector(withNotification:)), name: NSNotification.Name(rawValue: "LabelHasbeenUpdated"), object: nil)
            }
            else {
                self.perform(#selector(ViewController.fetchLocation), with: self, afterDelay: 1.0)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Check for Location access status
    func checkLocationAccessStatus() -> Bool {
        var notDetermined = true
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            notDetermined = true
        case .restricted:
            notDetermined = false
        case .denied:
            notDetermined = true
        case .authorizedAlways:
            notDetermined = false
        case .authorizedWhenInUse:
            notDetermined = false
        @unknown default: break
        }
        
        return notDetermined
    }
    
    @objc func fetchLocation() {
        LocationManager.sharedInstance.fetchData { (location2D, error) in
            let city = LocationManager.sharedInstance.stringCurrentCity
            let country = LocationManager.sharedInstance.stringCurrentCountry
            self.getCurrentLocationForecast(forCity: city, andCountry: country)
            self.getForcastForNextFiveDays(forCity: city, andCountry: country)
        }
    }
    
    @objc func handleLocationManagerSelector(withNotification notification : NSNotification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LabelHasbeenUpdated"), object: nil)
        if !checkLocationAccessStatus() {
            self.perform(#selector(ViewController.fetchLocation), with: self, afterDelay: 1.0)
        }
    }
    
    //MARK:-  Current location weather API call
    func getCurrentLocationForecast(forCity city: String, andCountry country: String) {
        var service = Service.init(httpMethod: WebserviceHTTPMethod.GET)
        service.url = ServiceHelper.getCurrentLocationForecastUrl(forState: city, country: country)
        ServiceManager.processDataFromServer(service: service, model: WeatherInfoResponseModel.self) { (responseVo, error) in
            //update UI
            self.currentInfoModel = responseVo
            DispatchQueue.main.async {
                Log.DLog(message: responseVo as AnyObject)
                self.updateUIWithCurrentInfoModel()
            }
            if error != nil {
                return
            }
        }
    }
    
    //MARK:-  Update UI according to Current location weather API response
    func updateUIWithCurrentInfoModel() {
        self.stateNameLabel.text = self.currentInfoModel?.name
        self.backgroundImg.isHidden = false
        if let tempValue = self.currentInfoModel?.main?.temp {
            let temp = Int((Double(tempValue) - 273.15))
            self.stateTempratureLabel.text = String(temp) + "°"
            self.currentTempLabel.attributedText = self.attributedStringValue(withTemp: String(temp), type: "Current")
        }
        if let minTempValue = self.currentInfoModel?.main?.temp_min {
            let temp = Int((Double(minTempValue) - 273.15))
            self.minTempLabel.attributedText = self.attributedStringValue(withTemp: String(temp), type: "min")
        }
        
        if let maxTempValue = self.currentInfoModel?.main?.temp_max {
            let temp = Int((Double(maxTempValue) - 273.15))
            self.maxTempLabel.attributedText = self.attributedStringValue(withTemp: String(temp), type: "max")
        }
        
        if let epochTime = self.currentInfoModel?.dt {
            let stringInDate = Utility.sharedInstance.convertEpochTimeToActualTime(epochTime: Double(epochTime))
            self.currentDayAndTime = epochTime
            let timeInfo = stringInDate.components(separatedBy: ",")
            self.currentDayLabel.text = timeInfo[0] + "(Today)"
            self.lastUpdateTimeLabel.text = "Last Updated: " + Utility.sharedInstance.convertEpochTimeToDate(epochTime: Double(epochTime)) //Utility.sharedInstance.getCurrentDateAndTimeString()
        }
        if let infoArr = self.currentInfoModel?.weather, let status = infoArr[0].main {
            self.tempratureIndicatorLabel.text = status
            if status == "Clouds" || status == "Mist" {
                self.backgroundImg.image = UIImage(named: "forest_cloudy")
                self.tableView.backgroundColor = UIColor(hexString: Contants.Colors.cloudyCode)
                self.view.backgroundColor = UIColor(hexString: Contants.Colors.cloudyCode)
            } else if status == "Haze" || status == "Rain" || status == "Smoke" || status == "Drizzle" {
                self.backgroundImg.image = UIImage(named: "forest_rainy")
                self.tableView.backgroundColor = UIColor(hexString: Contants.Colors.rainyCode)
                self.view.backgroundColor = UIColor(hexString: Contants.Colors.rainyCode)
            } else {
                //if status == "Sunny" || status == "Clear"
                self.backgroundImg.image = UIImage(named: "forest_sunny")
                self.tableView.backgroundColor = UIColor(hexString: Contants.Colors.sunnyCode)
                self.view.backgroundColor = UIColor(hexString: Contants.Colors.sunnyCode)
            }
        }
    }
    
    //MARK:- Make next five days weather API call
    func getForcastForNextFiveDays(forCity city: String, andCountry country: String) {
        var service = Service.init(httpMethod: WebserviceHTTPMethod.GET)
        service.url = ServiceHelper.getFiveDaysForecastUrl(forState: city, country: country)
        ServiceManager.processDataFromServer(service: service, model: ForecastInfoModel.self) { (responseModel, error) in
            DispatchQueue.main.async {
                Log.DLog(message: responseModel as AnyObject)
                self.forecastInfoModel = responseModel
                self.updateForecastInfoTable()
            }
            
            if error != nil {
                return
            }
        }
    }
    
    //MARK:-  Update UI according to next five days weather API response
    func updateForecastInfoTable() {
        var forecastItems =  [List]()
        var dateAtIndex: Date? = nil
        if let itemsArray = self.forecastInfoModel?.list {
            //Need to iterate because there is more than one entry for the same day(for every 3 hours)
            for item in itemsArray {
                if let epochTime = item.dt {
                    if dateAtIndex == nil {
                        dateAtIndex = Utility.sharedInstance.convertEpochTimeToActualDate(epochTime: Double(epochTime))
                        let cal = Calendar.current.compare(dateAtIndex!, to: Date(), toGranularity: Calendar.Component.day)
                        if cal == .orderedDescending {
                            forecastItems.append(item)
                        }
                    } else {
                        let loopDate = Utility.sharedInstance.convertEpochTimeToActualDate(epochTime: Double(epochTime))
                        if let date = dateAtIndex {
                            let cal = Calendar.current.compare(date, to: loopDate, toGranularity: Calendar.Component.day)
                            if cal == .orderedAscending {
                                dateAtIndex = nil
                            }
                        }
                    }
                }
            }
            self.displayItemsArray = forecastItems
            self.tableView.reloadData()
        }
    }
    
    //Get Attributed String
    func attributedStringValue(withTemp temp: String, type: String) -> NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: temp + "°\n",
                                                   attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 24)]);
        attrString.append(NSMutableAttributedString(string: type,
                                                    attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]));
        return attrString
    }
    
    //Reload UI with data model in Offline Mode
    func reloadViewWithOfflineData(withCurrentWeatherInfo curInfoModel: WeatherInfoResponseModel, andForecastInfo forInfoModel: ForecastInfoModel) {
        self.displayItemsArray.removeAll()
        self.currentInfoModel = curInfoModel
        self.forecastInfoModel = forInfoModel
        self.updateUIWithCurrentInfoModel()
        self.updateForecastInfoTable()
    }
    
    //MARK:- ButtonAction
    //Add Fav Button clicked
    @IBAction func addToFavButtonClicked(_ sender: Any) {
        var dict = ["currentInfo": self.currentInfoModel as Any, "forecastInfo": self.forecastInfoModel as Any, "isFav": ""] as [String : Any]
        var favStatus = ""
        if self.favItemsArray.count > 0 {
            let _ = self.favItemsArray.contains(where: { (item) -> Bool in
                if let infoModel = item["currentInfo"] as? WeatherInfoResponseModel, let cName = infoModel.name, let cityName = self.currentInfoModel?.name {
                    if cName != cityName  {
                        dict["isFav"] = "true" as Any
                        favStatus = "true"
                        self.addToFavButton.setTitle("UnFav", for: .normal)
                        Utility.sharedInstance.showToast(message: "Item added to fav list successfully")
                        self.favItemsArray.append(dict)
                        return true
                    } else {
                        self.favItemsArray.removeAll(where: { (obj) -> Bool in
                            if let objModel = obj["currentInfo"] as? WeatherInfoResponseModel, let objCityName = objModel.name, objCityName == cName {
                                favStatus = "false"
                                self.addToFavButton.setTitle("Fav", for: .normal)
                                Utility.sharedInstance.showToast(message: "Item Unfav Successfully")
                                return true
                            }
                            return false
                        })
                    }
                }
                return false
            })
        } else {
            self.addToFavButton.setTitle("UnFav", for: .normal)
            Utility.sharedInstance.showToast(message: "Item added to fav list successfully")
            dict["isFav"] = "true" as Any
            favStatus = "true"
            self.favItemsArray.append(dict)
        }
        
        //Save the Data
        let encoder = JSONEncoder()
        let model = WeatherInfoResponseModelOffline(withCurrent: self.currentInfoModel, withForecast: self.forecastInfoModel, withFav: favStatus)
        if let encodedData = try? encoder.encode(model) {
            UserDefaults.standard.set(encodedData, forKey: "FavItems")
        }
        UserDefaults.standard.synchronize()
    }
    
    //Show Fav list Button Clicked
    @IBAction func showFavListButtonClicked(_ sender: Any) {
        if self.favItemsArray.count > 0  {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let favVC = storyBoard.instantiateViewController(withIdentifier: "kFavListViewController") as! FavListViewController
            favVC.favListArray = self.favItemsArray
            favVC.delegateRefresh = self
            self.navigationController?.pushViewController(favVC, animated: true)
        } else {
            Utility.sharedInstance.showToast(message: "No Fav found")
        }
    }
    
    //Refresh data Button clicked
    @IBAction func refreshDataButtonClicked(_ sender: Any) {
        if Utility.isNetworkAvailable() {
            self.fetchLocation()
        } else {
            Utility.sharedInstance.showToast(message: "No Internet Access")
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayItemsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
            let item = self.displayItemsArray[indexPath.row]
            if let epoach = item.dt {
                let date = Utility.sharedInstance.convertEpochTimeToActualTime(epochTime: Double(epoach))
                let day = date.components(separatedBy: ",")
                cell.dayNameLabel.text = day[0]
            }
            if let weatherArr = item.weather, let status = weatherArr[0].main {
                if status == "Clouds" || status == "Mist" {
                    cell.weatherInfoIcon.image = UIImage(named: "clear")
                } else if status == "Haze" || status == "Rain" || status == "Smoke" || status == "Drizzle" {
                    cell.weatherInfoIcon.image = UIImage(named: "rain")
                } else {
                    //if status == "Sunny" || status == "Clear"
                    cell.weatherInfoIcon.image = UIImage(named: "partlysunny")
                }
            }
            if let info = item.main, let tempValue = info.temp {
                let temp = Int((Double(tempValue) - 273.15))
                cell.dayTempLabel.text = String(temp) + "°"
            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
}

extension ViewController: RefreshProtocol {
    func didSelectPlace(withData dictData: [String : Any]?) {
        if let placeData = dictData {
            if let curInfoModel = placeData["currentInfo"] as? WeatherInfoResponseModel,
                let forecastInfoModel = placeData["forecastInfo"] as? ForecastInfoModel  {
                self.reloadViewWithOfflineData(withCurrentWeatherInfo: curInfoModel, andForecastInfo: forecastInfoModel)
            }
        }
    }
}

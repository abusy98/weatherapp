//
//  FavListViewController.swift
//  WeatherApp
//
//  Created by Neelesh Rai on 01/12/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshProtocol {
    func didSelectPlace(withData dictData: [String: Any]?)
}

class FavListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var favListArray = [[String: Any]]()
    var delegateRefresh: RefreshProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.hidesBackButton = false
        self.navigationController?.navigationBar.tintColor = .blue
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Fav List"

    }
}

extension FavListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
            let item = self.favListArray[indexPath.row]
            if let itemAtIndex = item["currentInfo"] as? WeatherInfoResponseModel, let cityName = itemAtIndex.name {
                cell.dayNameLabel.text = cityName
                if let weatherArr = itemAtIndex.weather, let status = weatherArr[0].main {
                    if status == "Clouds" || status == "Mist" {
                        cell.backgroundColor = UIColor(hexString: Contants.Colors.cloudyCode)
                    } else if status == "Haze" || status == "Rain" || status == "Smoke" || status == "Drizzle" {
                        cell.backgroundColor = UIColor(hexString: Contants.Colors.rainyCode)
                    } else {
                        //if status == "Sunny" || status == "Clear"
                        cell.backgroundColor = UIColor(hexString: Contants.Colors.sunnyCode)
                    }
                }
                if let info = itemAtIndex.main, let tempValue = info.temp {
                    let temp = Int((Double(tempValue) - 273.15))
                    cell.dayTempLabel.text = String(temp) + "°"
                }
            }
            return cell
        }
        return UITableViewCell()
    }
}

extension FavListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if let delegate = self.delegateRefresh, let item = self.favListArray[indexPath.row] as? [String: Any] {
            delegate.didSelectPlace(withData: item)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//
//  ServiceManager.swift
//  Assignment
//
//  Created by Neelesh Rai on 05/05/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    
    class func processDataFromServer<T:Codable>(service: Service,model:T.Type,responseProcessingBlock: @escaping (T?,Error?) -> () )
    {
        if Utility.isNetworkAvailable() {
//                Utility.showLoader()
                let request = RequestManager.sharedInstance.createRequest(service: service)
                SessionManager.sharedInstance.processRequest(request: request) { (data, error) in
                        ServiceManager.processDataModalFromResponseData(model:T.self,data: data,error: error,responseProcessingBlock: responseProcessingBlock)
            
                }
        } else {
            Utility.sharedInstance.showToast(message: WeatherInfoAPIConstants.noNetworkError)
            let error: NSError = NSError.init(domain: "", code: 0,
                                              userInfo: [NSLocalizedDescriptionKey: WeatherInfoAPIConstants.noNetworkError])
            responseProcessingBlock(nil, error)
        }
    }
    
    private class func processDataModalFromResponseData<T:Codable>(model:T.Type, data:Data?,error:Error?,responseProcessingBlock: @escaping (T?,Error?) -> ())
    {
        Utility.hideLoader()
        if let responseError = error
        {
            responseProcessingBlock(nil,responseError)
            return
        }
        
        if let responseData = data
        {
            do{
                let jsonDecoder = JSONDecoder.init()
                let parsingModel = try jsonDecoder.decode(model.self, from: responseData)
                responseProcessingBlock(parsingModel, nil)
            }
            catch(let parsingError)
            {
                responseProcessingBlock(nil,parsingError)
            }
        }
    }
    
}

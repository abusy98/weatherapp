//
//  ServiceHelper.swift
//  Assignment
//
//  Created by Neelesh Rai on 05/05/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import Foundation

enum ServiceEnvironment : String {
    case Production   = "https://api.openweathermap.org/data/2.5/"
}

class WeatherInfoAPIConstants {
    static let apiKey = "&APPID=193f5aab24fc6d299c07a4a2e9438d4b"
    static let currentLocationServiceID = "weather?q="
    static let fiveDaysServiceID = "forecast?q="
    
    static let noNetworkError = "No network available"
}

class ServiceHelper: NSObject {
    static private var baseURL: ServiceEnvironment {
        get {
            return ServiceEnvironment.Production
        }
    }
}

//MARK: All Apis
extension ServiceHelper {
    static func getCurrentLocationForecastUrl(forState state: String, country:String) -> String {
        let currentLocationURLString = WeatherInfoAPIConstants.currentLocationServiceID + state + "," + country
        return baseURL.rawValue + currentLocationURLString + WeatherInfoAPIConstants.apiKey
    }
    static func getFiveDaysForecastUrl(forState state: String, country:String) -> String {
        let forecastURLString = WeatherInfoAPIConstants.fiveDaysServiceID + state + "," + country
        return baseURL.rawValue + forecastURLString + WeatherInfoAPIConstants.apiKey
    }
}

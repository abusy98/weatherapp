//
//  WebserviceParameter.swift
//  Assignment
//
//  Created by Neelesh Rai on 05/05/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import Foundation


enum WebserviceHTTPMethod: String {
    case GET  =   "GET"
    case POST =   "POST"
}

struct Service {
    var url: String!
    var httpMethod : WebserviceHTTPMethod
    var params : [String: Any]
    var headers : [String: Any]
    
    init(httpMethod :WebserviceHTTPMethod) {
        self.httpMethod = httpMethod
        self.params = [String: Any]()
        self.headers = [HTTPHeaderKey.HTTPHeaderKeyAccept.rawValue:HTTPHeaderValue.HTTPHeaderValueApplicationJSON.rawValue,HTTPHeaderKey.HTTPHeaderKeyContenttype.rawValue:HTTPHeaderValue.HTTPHeaderValueApplicationFormURLEncoded.rawValue]
    }
}

//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Neelesh Rai on 27/11/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


class LocationManager: NSObject, CLLocationManagerDelegate {
    var locationManager    = CLLocationManager()
    var locationCoordinate = CLLocationCoordinate2D()
    
    var location           : NSString = ""
    var stringCurrentCity  : String   = ""
    var stringCurrentCountry : String   = ""
    var onFetchComplete: ((CLLocationCoordinate2D?, Error?) -> ())?
    var fetchCompleteAddress: ((String?, Error?) -> ())?
    
    typealias ServiceResponse = (CLLocationCoordinate2D?, Error?) -> Void
    typealias addressResponse = (String?, Error?) -> Void
    
    // MARK: Shared Instance
    class  var sharedInstance: LocationManager
    {
        struct Singleton
        {
            static let instance = LocationManager()
        }
        return Singleton.instance
    }
    
    func initLocationManager()
    {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        //self.locationCoordinate = (manager.location?.coordinate)!
        //locationManager.stopUpdatingLocation()
        if let callback = self.onFetchComplete {
            callback(nil, error)
            self.onFetchComplete = nil
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0] as CLLocation
        let locValue:CLLocationCoordinate2D = userLocation.coordinate
        self.locationCoordinate = userLocation.coordinate
        self.reverseGeocode(locValue.latitude, long: locValue.longitude)
        manager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var shouldAllow = false
        switch status {
        case CLAuthorizationStatus.restricted:
            print("Restricted Access to location")
        case CLAuthorizationStatus.denied:
            print("User denied access to location")
        case CLAuthorizationStatus.notDetermined:
            print("Status not determined")
        default:
            print("Allowed to location Access")
            shouldAllow = true
        }
        
        if (shouldAllow == true) {
            NSLog("Location to Allowed")
            // Start location services
            NotificationCenter.default.post(name: Notification.Name(rawValue: "LabelHasbeenUpdated"), object: status)
            locationManager.startUpdatingLocation()
        } else {
            
        }
    }
    
    func reverseGeocode(_ lat:CLLocationDegrees, long: CLLocationDegrees) {
        let latitude  = self.locationManager.location?.coordinate.latitude
        let longitude = self.locationManager.location?.coordinate.longitude
        if latitude != nil && longitude != nil{
            let location = CLLocation(latitude: latitude!, longitude: longitude!) //changed!!!
            print(location)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                print(location)
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                if placemarks!.count > 0 {
                    let pm                  = placemarks![0]
                    let array               = pm.addressDictionary!["FormattedAddressLines"]
                    self.stringCurrentCity  = pm.locality!
                    self.stringCurrentCountry = pm.country!
                    self.location           = ((array as AnyObject).object(at: 1)) as! NSString
                    if let callback = self.onFetchComplete {
                        callback(location.coordinate  , nil)
                        self.onFetchComplete = nil
                    }
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
    }
    
    
    
    func getAddressFromLatLon(_ lat:CLLocationDegrees, long: CLLocationDegrees) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = long
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    if let callback = self.fetchCompleteAddress {
                        callback(nil, error)
                    }
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country as Any)
                    print(pm.locality as Any)
                    print(pm.subLocality as Any)
                    print(pm.thoroughfare as Any)
                    print(pm.postalCode as Any)
                    print(pm.subThoroughfare as Any)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                    if let callback = self.fetchCompleteAddress {
                        callback(addressString, nil)
                    }
                }
        })
        
    }
    
    
    func getLatLongValues()-> (CLLocationCoordinate2D) {
        return self.locationManager.location!.coordinate
    }
    
    func getLatLong()-> (lat: CLLocationDegrees, long: CLLocationDegrees) {
        if ((self.locationManager.location?.coordinate.latitude) != nil) {
            return ((self.locationManager.location?.coordinate.latitude)!, (self.locationManager.location?.coordinate.longitude)!)
        }
        return (0.0,0.0)
    }
    
    func getLocationAddress() -> NSString {
        return self.location
    }
    
    func getCurrentStateAndCity() -> (state:String, city:String) {
        return (self.stringCurrentCountry, self.stringCurrentCity)
    }
    
    func fetchData( _ completion: @escaping ServiceResponse) -> Void {
        onFetchComplete = completion
        self.locationManager.startUpdatingLocation()
    }
    
    func fetchCompleteAddress( _ completion: @escaping addressResponse) -> Void {
        fetchCompleteAddress = completion
        self.getAddressFromLatLon((self.locationCoordinate.latitude), long: (self.locationCoordinate.longitude))
    }
}


//
//  Utility.swift
//  WeatherApp
//
//  Created by Neelesh Rai on 27/11/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import SystemConfiguration
import ReachabilitySwift


class Log {
    static  func DLog(message: AnyObject, function: String = #function) {
        #if DEBUG
        print("\(function): \(message)")
        #endif
    }
}

class Utility {
    static let sharedInstance = Utility()
    static private var activityIndicator: MBProgressHUD?
    private var toastLabel: UILabel?
    
    var reachability: Reachability!
    private init() {}
    
    
    // MARK: - Loader Methods
    static func showLoader() {
        DispatchQueue.main.async {
            if activityIndicator == nil {
                if let window = AppDelegate.sharedInstance.window {
                    activityIndicator = MBProgressHUD.showAdded(to: window, animated: true)
                    activityIndicator?.label.text = "Loading..."
                }
            } else {
                hideLoader()
                showLoader()
            }
        }
    }
    
    static func hideLoader() {
        DispatchQueue.main.async {
            activityIndicator?.hide(animated: true)
            activityIndicator = nil
        }
    }
    
    // MARK: - Show Alert
    static func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let topController = self.getTopViewController()
            if topController is UIAlertController {
            } else {
                let alert = UIAlertController(title: title, message: message,
                                              preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                topController.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Network Methods
    static func isNetworkAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func startNetworkNotifier() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotification, object: reachability)
        reachability = Reachability.init()
        do {
            try reachability.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
    }
    @objc func reachabilityChanged(note: Notification) {
        let rrr = note.object as? Reachability
        if (rrr?.isReachable)! {
            
        } else {
            Utility.hideLoader()
        }
    }
    
    func stopMonitoring() {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
    // MARK: - Set View CornerRadius,BorderWidth,Color
    class func setView(view : UIView, cornerRadius: CGFloat, borderWidth : CGFloat, color : UIColor)
    {
        view.layer.cornerRadius = cornerRadius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color.cgColor
        view.layer.masksToBounds = true
    }
    
    // MARK: - Show Toast
    func showToast(message: String) {
        DispatchQueue.main.async {
            if self.toastLabel != nil {
                NSObject.cancelPreviousPerformRequests(withTarget: self,
                                                       selector: #selector(self.addToastLabel(message:)), object: nil)
                self.toastLabel = nil
            }
            self.addToastLabel(message: message)
        }
    }
    
    @objc private func addToastLabel(message: String) {
        let font = UIFont(name: "Helvetica Neue", size: 12.0)
        let size: CGSize = Utility.getSize(message, font: font!)
        self.toastLabel =
            
            UILabel(frame: CGRect(x: UIScreen.main.bounds.width/2 - size.width/2,
                                  y: UIScreen.main.bounds.height-150, width: size.width + 10, height: 35))
        self.toastLabel?.backgroundColor = UIColor.black
        self.toastLabel?.textColor = UIColor.white
        self.toastLabel?.textAlignment = .center
        self.toastLabel?.font = font
        self.toastLabel?.text = message
        self.toastLabel?.alpha = 1.0
        self.toastLabel?.layer.cornerRadius = 10
        self.toastLabel?.numberOfLines = 0
        self.toastLabel?.clipsToBounds  =  true
        AppDelegate.sharedInstance.window?.addSubview(self.toastLabel!)
        UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.toastLabel?.alpha = 0.0
        }, completion: {_ in
            self.toastLabel?.removeFromSuperview()
            self.toastLabel = nil
        })
    }
    
    // MARK: - Calculate Text Size
    static func getSize(_ text: String, font: UIFont) -> CGSize {
        let size = CGSize(width: 220, height: 20000.0)
        let attributedText = NSAttributedString(string: text.removingPercentEncoding ?? "",
                                                attributes: [NSAttributedString.Key.font: font])
        let rect: CGRect = attributedText.boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)
        return rect.size
    }
    
    // MARK: - Get ViewController From Storyboard
    static func getViewController<T:UIViewController>(ofType viewController:T.Type) -> T
    {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: type(of: T()))) as! T
    }
    
    static func getTopViewController<T: UIViewController>() -> T {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController as! T
            // topController should now be your topmost view controller
        }
        return UIViewController() as! T
    }
    
    //Convert Epoch time to actual time
    func convertEpochTimeToActualTime(epochTime: Double) -> String {
        let date = Date(timeIntervalSince1970: epochTime)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.full //Set date style
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    //Convert Epoch time to Date time Format
    func convertEpochTimeToActualDate(epochTime: Double) -> Date {
        let date = Date(timeIntervalSince1970: epochTime)
        return date
    }
    
    func convertEpochTimeToDate(epochTime: Double) -> String {
        let date = Date(timeIntervalSince1970: epochTime)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-YYYY, hh:mm a"
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
}

struct Contants {
    struct Colors {
        static let sunnyCode = "#47AB2F"
        static let cloudyCode = "#54717A"
        static let rainyCode = "#57575D"
    }
}

extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
}

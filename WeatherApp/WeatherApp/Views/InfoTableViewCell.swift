//
//  InfoTableViewCell.swift
//  WeatherApp
//
//  Created by Neelesh Rai on 27/11/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import Foundation
import UIKit

class InfoTableViewCell : UITableViewCell {
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var weatherInfoIcon: UIImageView!
    @IBOutlet weak var dayTempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

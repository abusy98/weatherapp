//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Neelesh Rai on 27/11/19.
//  Copyright © 2019 Neelesh Rai. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherAppTests: XCTestCase {
    var mainController: ViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let mainNav = storyBoard.instantiateViewController(withIdentifier: "kMainNav") as? UINavigationController
        mainController = mainNav?.viewControllers[0] as? ViewController
        mainController.loadView()
        
        if let jsonData = self.loadJsonForCurrentWeatherInfo(filename: "weatherInfoData") {
            mainController.currentInfoModel = jsonData
        }
    }
    
    func testViewDidLoad(){
        mainController.viewDidLoad()
    }
    
    func testViewWillAppear(){
        mainController.viewWillAppear(true)
        XCTAssertNotNil(mainController.tableView.tableFooterView)
        let isNavHidden = mainController.navigationController?.navigationBar
        XCTAssertTrue((isNavHidden != nil))
    }
    
    func testCheckLocationAccessStatus() {
        let hasaccess = mainController.checkLocationAccessStatus()
        XCTAssertNotNil(hasaccess)
    }
    
    func testUpdateUIWithCurrentInfoModel() {
        mainController.updateUIWithCurrentInfoModel()
        _ = mainController.stateNameLabel.text
        XCTAssertNotNil(mainController.stateNameLabel.text)
    }
    
    func testGetCurrentLocationForecast() {
        mainController.getCurrentLocationForecast(forCity: "Johannesburg", andCountry: "SouthAfrica")
        mainController.currentInfoModel = nil
        if let jsonData = self.loadJsonForCurrentWeatherInfo(filename: "weatherInfoData") {
            mainController.currentInfoModel = jsonData
            _ = mainController.stateNameLabel.text
            XCTAssertNotNil(mainController.stateNameLabel.text)
        }
        else {
            XCTAssertNil(mainController.currentInfoModel, "No data to show")
        }
    }
    
    func testGetForcastForNextFiveDays(){
        mainController.getForcastForNextFiveDays(forCity: "Johannesburg", andCountry: "SouthAfrica")
        mainController.forecastInfoModel = nil
        if let jsonData = self.loadJsonForForecastData(filename: "forcastInfoData") {
            mainController.forecastInfoModel = jsonData
            let itemsArray = mainController.forecastInfoModel?.list
            XCTAssertNotNil(itemsArray)
        } else {
            XCTAssertNil(mainController.forecastInfoModel, "No data to show")
        }
    }
    
    func testAttributedStringValue() {
        let newString = mainController.attributedStringValue(withTemp: "30", type: "max")
        XCTAssertNotNil(newString)
    }
    

    func testNumberOfRowsInTableView() {
        testGetForcastForNextFiveDays()
        mainController.updateForecastInfoTable()
        let rows = mainController.tableView(mainController.tableView, numberOfRowsInSection: 0)
        XCTAssertNotNil(rows)
    }
    
    func testCellForRowAtIndexInTableView() {
        testGetForcastForNextFiveDays()
        mainController.updateForecastInfoTable()
        let cell = mainController.tableView(mainController.tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell.isKind(of: InfoTableViewCell.self))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        mainController = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func loadJsonForCurrentWeatherInfo(filename fileName: String) -> WeatherInfoResponseModel? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(WeatherInfoResponseModel.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    func loadJsonForForecastData(filename fileName: String) -> ForecastInfoModel? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(ForecastInfoModel.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
